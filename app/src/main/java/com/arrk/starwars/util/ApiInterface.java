package com.arrk.starwars.util;

import com.arrk.starwars.pojos.Characters;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public interface ApiInterface
{
    @GET("people")
    Call<Characters> getStarWarsCharacters();

    @GET
    Call<Characters> getNextCharacters(@Url String nextURL);
}