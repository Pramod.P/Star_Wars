package com.arrk.starwars.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arrk.starwars.R;
import com.arrk.starwars.characters.CharacterActivity;
import com.arrk.starwars.pojos.CharacterPojo;

import java.util.ArrayList;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>
{
    ArrayList<CharacterPojo> characterModelArrayList;
    MyViewHolder myViewHolder;
    Context context;


    public RecyclerAdapter(ArrayList<CharacterPojo> characterModelArrayList, Context context)
    {
        this.characterModelArrayList = characterModelArrayList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.character_item, parent, false);
        myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        myViewHolder.tvName.setText(characterModelArrayList.get(position).getName());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ((CharacterActivity) context).showDetails(characterModelArrayList.get(position));
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvName;

        public MyViewHolder(View view)
        {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvCharacterName);
        }
    }

    @Override
    public int getItemCount()
    {
        if(characterModelArrayList != null && characterModelArrayList.size()>0)
            return characterModelArrayList.size();
        else
            return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void notifyData(ArrayList<CharacterPojo> characterModelArrayList)
    {
        this.characterModelArrayList = characterModelArrayList;
        notifyDataSetChanged();
    }
}
