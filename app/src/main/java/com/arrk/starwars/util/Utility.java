package com.arrk.starwars.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.widget.SwipeRefreshLayout;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public class Utility
{
    public static final String CHARACTER_POJO = "character_pojo";
    public ProgressDialog progressDialog;

    public void showProgressDialog(Context context) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(context);

        if (!progressDialog.isShowing())
        {
            progressDialog.setMessage("Please wait...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    public void dismissDialog()
    {
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public void showTryAgainDialog(Context context, final DialogListeners listener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Star Wars");
        builder.setMessage("Some problem occurred while retrieving data from the server.");
        builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onTryAgain(dialog);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onCancel(dialog);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

   public interface DialogListeners
    {
        void onTryAgain(DialogInterface dialog);
        void onCancel(DialogInterface dialog);
    }

    public void internetErrorDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Star Wars");
        builder.setMessage("Please connect to working internet connection.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dismissDialog();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void dismissSwipeLayout(SwipeRefreshLayout swipeRefreshLayout)
    {
        if(swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }
}
