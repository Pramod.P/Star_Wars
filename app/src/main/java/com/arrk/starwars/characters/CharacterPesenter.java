package com.arrk.starwars.characters;

import com.arrk.starwars.pojos.Characters;
import com.arrk.starwars.util.ConnectivityReceiver;

import retrofit2.Response;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public class CharacterPesenter implements CharacterContract.CPresenter
{
    public CharacterContract.CView cView;
    public CharacterModel characterModel;

    public CharacterPesenter(CharacterContract.CView cView)
    {
        this.cView = cView;
        characterModel = new CharacterModel(this);
    }

    @Override
    public void fetchData()
    {
        if(ConnectivityReceiver.isConnected())
            characterModel.getDataFromServer();
        else
            cView.internetConnectionError();
    }

    @Override
    public void onSucess(Response<Characters> response)
    {
        Characters characters = response.body();
        cView.displayData(characters);
    }

    @Override
    public void onFailure(Throwable t)
    {
        cView.displayError(t);
    }

    @Override
    public void loadNextData(String nextURL) {
        if(ConnectivityReceiver.isConnected())
            characterModel.getNextData(nextURL);
        else
            cView.internetConnectionError();
    }

    @Override
    public void onNoInternet() {
        cView.internetConnectionError();
    }
}
