package com.arrk.starwars.characters;

import com.arrk.starwars.pojos.Characters;

import retrofit2.Response;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public interface CharacterContract
{
    interface CModel
    {
         void getDataFromServer();
         void getNextData(String url);
    }

    interface CView
    {
        void displayData(Characters characters);
        void internetConnectionError();
        void displayError(Throwable throwable);
    }

    interface CPresenter
    {
        void fetchData();
        void loadNextData(String nextURL);
        void onSucess(Response<Characters> response);
        void onFailure(Throwable throwable);
        void onNoInternet();
    }
}
