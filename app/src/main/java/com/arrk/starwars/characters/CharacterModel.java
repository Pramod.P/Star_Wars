package com.arrk.starwars.characters;


import android.util.Log;

import com.arrk.starwars.pojos.CharacterPojo;
import com.arrk.starwars.pojos.Characters;
import com.arrk.starwars.util.ApiClient;
import com.arrk.starwars.util.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public class CharacterModel implements CharacterContract.CModel
{
    public static String TAG = CharacterModel.class.getSimpleName();

    CharacterPesenter presenter;

    public CharacterModel(CharacterPesenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void getDataFromServer()
    {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Characters> call = apiService.getStarWarsCharacters();
        call.enqueue(new Callback<Characters>() {

            @Override
            public void onResponse(Call<Characters>call, Response<Characters> response)
            {
                if(response != null)
                {
                    List<CharacterPojo> characterPojoList = response.body().getResults();
                    Log.d(TAG, "Number of characters : " + characterPojoList.size());
                    presenter.onSucess(response);
                }
            }

            @Override
            public void onFailure(Call<Characters>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                presenter.onFailure(t);
            }
        });
    }

    @Override
    public void getNextData(String nextURL)
    {
        Log.d("URL",nextURL);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Characters> call = apiService.getNextCharacters(nextURL);
        call.enqueue(new Callback<Characters>() {

            @Override
            public void onResponse(Call<Characters>call, Response<Characters> response)
            {
                if(response != null)
                {
                    List<CharacterPojo> characterPojoList = response.body().getResults();
                    Log.d(TAG, "Number of characters : " + characterPojoList.size());
                    presenter.onSucess(response);
                }
            }

            @Override
            public void onFailure(Call<Characters>call, Throwable t) {
                Log.e(TAG, t.toString());
                presenter.onFailure(t);
            }
        });
    }
}
