package com.arrk.starwars.characters;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import com.arrk.starwars.CharacterDetailActivity;
import com.arrk.starwars.R;
import com.arrk.starwars.pojos.CharacterPojo;
import com.arrk.starwars.pojos.Characters;
import com.arrk.starwars.util.Utility;
import com.arrk.starwars.util.RecyclerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public class CharacterActivity extends AppCompatActivity implements CharacterContract.CView
{
    @BindView(R.id.recyclerCharacters)
        RecyclerView recyclerView;
    @BindView(R.id.swipeRefresh)
            SwipeRefreshLayout swipeRefreshLayout;

    ArrayList<CharacterPojo> characterPojoArrayList;
    RecyclerAdapter recyclerAdapter;
    LinearLayoutManager layoutManager;
    CharacterPesenter presenter;
    String nextURL = "", previousURL="";
    Utility utility;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.characters_activity);
        ButterKnife.bind(this);
        init();
    }

    public void init()
    {
        presenter = new CharacterPesenter(this);
        utility = new Utility();
        utility.showProgressDialog(this);
        presenter.fetchData();
        setListData();

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if(dy>0) {
                    if ((firstVisibleItemPosition + visibleItemCount) >= totalItemCount ) {
                        if(!TextUtils.isEmpty(nextURL) && !nextURL.equalsIgnoreCase("null") && !previousURL.equalsIgnoreCase(nextURL))
                        {
                            previousURL = nextURL;
                            utility.showProgressDialog(CharacterActivity.this);
                            presenter.loadNextData(nextURL);
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                setListData();
                nextURL = "";
                previousURL = "";
                presenter.fetchData();
            }
        });
    }

    public void setListData()
    {
        characterPojoArrayList = new ArrayList<>();

        recyclerAdapter = new RecyclerAdapter(characterPojoArrayList,this);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public void displayData(Characters characters)
    {
        utility.dismissDialog();
        nextURL = characters.getNext()+"";
        if(characters.getResults().size()>0) {
            for(int i=0;i<characters.getResults().size();i++) {
                characterPojoArrayList.add(characters.getResults().get(i));
            }
            recyclerAdapter.notifyData(characterPojoArrayList);
        }

        utility.dismissSwipeLayout(swipeRefreshLayout);
    }

    @Override
    public void displayError(Throwable throwable)
    {
        previousURL = "";
        utility.dismissDialog();
        new Utility().showTryAgainDialog(this, new Utility.DialogListeners() {
            @Override
            public void onTryAgain(DialogInterface dialog) {
                dialog.dismiss();
                if(!TextUtils.isEmpty(nextURL)) {
                    utility.showProgressDialog(CharacterActivity.this);
                    presenter.loadNextData(nextURL);
                }
                else{
                    utility.showProgressDialog(CharacterActivity.this);
                    presenter.fetchData();
                }
            }

            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                utility.dismissDialog();
                utility.dismissSwipeLayout(swipeRefreshLayout);
            }
        });
    }

    @Override
    public void internetConnectionError()
    {
        previousURL="";
        utility.internetErrorDialog(this);
        utility.dismissSwipeLayout(swipeRefreshLayout);
    }

    public void showDetails(CharacterPojo characterPojo)
    {
        Intent intent = new Intent(this,CharacterDetailActivity.class);
        intent.putExtra(Utility.CHARACTER_POJO,characterPojo);
        startActivity(intent);
    }
}
