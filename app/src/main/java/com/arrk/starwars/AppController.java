package com.arrk.starwars;

import android.app.Application;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public class AppController extends Application
{
    static AppController mInstance;

    @Override
    public void onCreate() {
        mInstance = this;
        super.onCreate();
    }

    public static synchronized AppController getInstance()
    {
        return mInstance;
    }

}
