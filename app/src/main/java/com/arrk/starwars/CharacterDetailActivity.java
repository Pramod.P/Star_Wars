package com.arrk.starwars;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.arrk.starwars.R;
import com.arrk.starwars.pojos.CharacterPojo;
import com.arrk.starwars.util.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public class CharacterDetailActivity extends AppCompatActivity
{
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvHeight)
    TextView tvHeight;
    @BindView(R.id.tvMass)
    TextView tvMass;
    @BindView(R.id.tvRecordDate)
    TextView tvRecordDate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_back_white);

        ButterKnife.bind(this);

        if(getIntent()!=null)
        {
            CharacterPojo characterPojo = (CharacterPojo) getIntent().getSerializableExtra(Utility.CHARACTER_POJO);
            if(characterPojo != null)
            {
                tvName.setText(characterPojo.getName());
                tvHeight.setText(characterPojo.getHeight());
                tvMass.setText(characterPojo.getMass());
                tvRecordDate.setText(getDate(characterPojo.getCreated()));
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        onBackPressed();
        return true;
    }

    public String getDate(String orgDate)
    {
        String strDate = orgDate.split("T")[0];
        String[] arrTime = orgDate.split("T")[1].split(":");
        String time = arrTime[0]+":"+arrTime[1];

        return strDate+" "+time;
    }
}
